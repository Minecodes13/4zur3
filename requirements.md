# NPM Requirements
## Microsoft/Github
+ Electron - by electron and electron-nightly
+ Typescript - by typescript-bot, weswigham, sanders_n, andrewbranch, minestarks, rbuckton, sheetalkamat, orta and typescript-deploys
+ electron-builder - by stefanjudis and develar
+ electron-builder-squirrel-windows - by develar
## Others
+ npm-run-all - by mysticatea
# Fonts(Page: [fonts.google.com](https://fonts.google.com))
+ Truculenta - by Omnibus-Type
+ Roboto - by Christian Robertson
+ Roboto Mono - by Christian Robertson
+ JetBrains Mono - by JetBrains, Philipp Nurullin and Konstantin Bulenkov