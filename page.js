const express = require("express");
const app = express();

exports.server = () => {
    console.log("Window pages on 127.0.0.1:9999");
    app.use(express.static('src'));
    app.listen(9999);
}