import { app, BrowserWindow, Menu, shell } from 'electron';
import { server } from "./page.js";

function createWindow () {
  const win = new BrowserWindow({
    width: 300,
    height: 550,
    frame: true,
    autoHideMenuBar: true,
    title: "",
    maximizable: false,
    hasShadow: false,
    center: true,
    icon: "img/icon.png",
    webPreferences: {
      nodeIntegration: true
    }
  });
  win.loadURL("https://minecodes13.gitlab.io/4zur3/index.html");
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
  process.exit(0);
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})