"use strict";
exports.__esModule = true;
var electron_1 = require("electron");
function createWindow() {
    var win = new electron_1.BrowserWindow({
        width: 300,
        height: 550,
        frame: true,
        autoHideMenuBar: true,
        title: "",
        maximizable: false,
        hasShadow: false,
        center: true,
        icon: "img/icon.png",
        webPreferences: {
            nodeIntegration: true
        }
    });
    win.loadURL("https://minecodes13.gitlab.io/4zur3/index.html");
}
electron_1.app.whenReady().then(createWindow);
electron_1.app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        electron_1.app.quit();
    }
    process.exit(0);
});
electron_1.app.on('activate', function () {
    if (electron_1.BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});
